package restAssured.events.test;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import restAssured.events.utility.BaseUtility;

public class OrderTest extends BaseUtility {
	
	String eventId = "1";
	String orderEndPoint = "/order-statistics";

	@Test(description = "To get order statistics by event")
	public void displayOrderStatisticsByEvent() {
		response =RestAssured.given().
							header("Authorization", "JWT " + getAccessToken()).
							accept("application/vnd.api+json").
							get(RestAssured.baseURI + "/v1/events/"+eventId+orderEndPoint);
		Reporter.log(response.getBody().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
	}
	
	@Test(description = "To get order statistics by ticket")
	public void displayOrderStatisticsByTicket() {
		response =RestAssured.given().
							header("Authorization", "JWT " + getAccessToken()).
							accept("application/vnd.api+json").
							get(RestAssured.baseURI + "/v1/events/"+eventId+orderEndPoint);
		Reporter.log(response.getBody().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
	}
}
