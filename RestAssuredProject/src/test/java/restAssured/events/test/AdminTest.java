package restAssured.events.test;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import restAssured.events.utility.BaseUtility;

public class AdminTest extends BaseUtility {
	
	@Test(description = "To get event statistics details")
	public void getEventStatistics() {
		response =RestAssured.given().
				header("Authorization", "JWT " + getAccessToken()).
				accept("application/vnd.api+json").
				get(RestAssured.baseURI + "/v1/admin/statistics/events");
		Reporter.log(response.getBody().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
	}
	
	@Test(description = "To get event type statistics details")
	public void getEventTypeStatistics() {
		response =RestAssured.given().
				header("Authorization", "JWT " + getAccessToken()).
				accept("application/vnd.api+json").
				get(RestAssured.baseURI + "/v1/admin/statistics/event-types");
		Reporter.log(response.getBody().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
	}
	
	@Test(description = "To get event topic statistics details")
	public void getEventTopicStatistics() {
		response =RestAssured.given().
				header("Authorization", "JWT " + getAccessToken()).
				accept("application/vnd.api+json").
				get(RestAssured.baseURI + "/v1/admin/statistics/event-topics");
		Reporter.log(response.getBody().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
	}
	
	@Test(description = "To get user statistics details")
	public void getUserStatistics() {
		response =RestAssured.given().
				header("Authorization", "JWT " + getAccessToken()).
				accept("application/vnd.api+json").
				get(RestAssured.baseURI + "/v1/admin/statistics/users");
		Reporter.log(response.getBody().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
	}
	
	@Test(description = "To get session statistics details")
	public void getSessionStatistics() {
		response =RestAssured.given().
				header("Authorization", "JWT " + getAccessToken()).
				accept("application/vnd.api+json").
				get(RestAssured.baseURI + "/v1/admin/statistics/sessions");
		Reporter.log(response.getBody().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
	}
	
	@Test(description = "To get mail statistics details")
	public void getMailStatistics() {
		response =RestAssured.given().
				header("Authorization", "JWT " + getAccessToken()).
				accept("application/vnd.api+json").
				get(RestAssured.baseURI + "/v1/admin/statistics/mails");
		Reporter.log(response.getBody().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
	}
}
