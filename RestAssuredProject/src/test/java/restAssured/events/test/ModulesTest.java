package restAssured.events.test;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import restAssured.events.utility.BaseUtility;

public class ModulesTest extends BaseUtility {
	public int moduleId;
	public String moduleEndPoint = "/v1/modules?sort=app-name&filter=[]";

	@Test(description = "To get module details")
	public void getModuleDetails() {
		response =RestAssured.given().
						header("Authorization", "JWT " + getAccessToken()).
						contentType("application/vnd.api+json").
						get(RestAssured.baseURI + moduleEndPoint);
		Assert.assertEquals(response.getStatusCode(), 200);
		moduleId = response.jsonPath().getInt("data.id");
		Reporter.log("module id is :: "+moduleId);
	}
}