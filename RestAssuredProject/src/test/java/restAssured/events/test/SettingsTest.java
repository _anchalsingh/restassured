package restAssured.events.test;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import restAssured.events.utility.BaseUtility;

public class SettingsTest extends BaseUtility {
	public int settingId = 1;

	@Test(description = "To get settings details")
	public void getSettingDetails() {
		response =RestAssured.given().
				header("Authorization", "JWT " + getAccessToken()).
				contentType("application/vnd.api+json").
				accept("application/vnd.api+json").
				get(RestAssured.baseURI + "/v1/settings?sort=app-name&filter=[]");
		String jsonBody = response.getBody().asString();
		Reporter.log(jsonBody);
		Assert.assertEquals(response.getStatusCode(), 200);
		settingId = response.jsonPath().getInt("data.id");
		Reporter.log("settings id :: "+settingId);
	}

}
