package restAssured.events.test;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import net.minidev.json.JSONObject;
import restAssured.events.utility.BaseUtility;

public class EventExportTest extends BaseUtility {
	
	JSONObject jsonData = new JSONObject();
	JSONObject requestJson = new JSONObject();
	String taskURL ="";
	String eventID = "1";
	
	@Test(description = "Verify user is able to start a task of exporting the event in the zip format.")
	public void startEventExportAsZipFileFormat() {
		jsonData.put("image", true);
		jsonData.put("video", true);
		jsonData.put("document", true);
		jsonData.put("audio",true);
		requestJson.put("", jsonData);
		Reporter.log(jsonData.toJSONString());
		response =RestAssured.given().
							header("Authorization", "JWT " + getAccessToken()).
							contentType("application/vnd.api+json").
							body(requestJson).
							post(RestAssured.baseURI + "/v1/events/"+eventID+"/export/json");
		Assert.assertEquals(response.getStatusCode(), 200);
		taskURL = response.jsonPath().get("task_url");
		Reporter.log("Task URL is :: "+taskURL);
	}
	
	@Test(description = "Verify user is able to start export a particular event in the ical format")
	public void startEventExportAsICALFileFormat() {
		response =RestAssured.given().
							header("Authorization", "JWT " + getAccessToken()).
							get(RestAssured.baseURI + "/v1/events/"+eventID+"/export/ical");
		Assert.assertEquals(response.getStatusCode(), 200);
		taskURL = response.jsonPath().get("task_url");
		Reporter.log("Task URL is : "+taskURL);
	}
	
	@Test(description = "Verify user is able to start export a particular event in the xcal format")
	public void startEventExportAsXCalenderFileFormat() {
		response =RestAssured.given().
							header("Authorization", "JWT " + getAccessToken()).
							get(RestAssured.baseURI + "/v1/events/"+eventID+"/export/xcal");
		Assert.assertEquals(response.getStatusCode(), 200);
		taskURL = response.jsonPath().get("task_url");
		Reporter.log("Task URL is : "+taskURL);
	}
	
	@Test(description = "Verify user is able to start export a particular event in the csv format")
	public void startEventExportCSVFileFormat() {
		response =RestAssured.given().
							header("Authorization", "JWT " + getAccessToken()).
							get(RestAssured.baseURI + "/v1/events/"+eventID+"/export/orders/csv");
		Assert.assertEquals(response.getStatusCode(), 200);
		taskURL = response.jsonPath().get("task_url");
		Reporter.log("Task URL is : "+taskURL);
	}
	
	@Test(description = "Verify user is able to start export a particular event in the pdf format")
	public void startEventExportPDFFileFormat() {
		response =RestAssured.given().
							header("Authorization", "JWT " + getAccessToken()).
							get(RestAssured.baseURI + "/v1/events/"+eventID+"/export/orders/pdf");
		Assert.assertEquals(response.getStatusCode(), 200);
		taskURL = response.jsonPath().get("task_url");
		Reporter.log("Task URL is : "+taskURL);
	}
	
	@Test(description = "Verify user is able to start sessions export as csv ")
	public void startEventSessionExportAsCSVFileFormat() {
		response =RestAssured.given().
							header("Authorization", "JWT " + getAccessToken()).
							get(RestAssured.baseURI + "/v1/events/"+eventID+"/export/sessions/csv");
		Assert.assertEquals(response.getStatusCode(), 200);
		taskURL = response.jsonPath().get("task_url");
		Reporter.log("Task URL is : "+taskURL);
	}
	
	@Test(description = "Verify user is able to start speakers export as csv ")
	public void startEventSpeakersExportAsCSVFileFormat() {
		response =RestAssured.given().
							header("Authorization", "JWT " + getAccessToken()).
							get(RestAssured.baseURI + "/v1/events/"+eventID+"/export/speakers/csv");
		Assert.assertEquals(response.getStatusCode(), 200);
		taskURL = response.jsonPath().get("task_url");
		Reporter.log("Task URL is : "+taskURL);
	}
}
