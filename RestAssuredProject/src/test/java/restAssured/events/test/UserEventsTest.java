package restAssured.events.test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.minidev.json.JSONObject;
import restAssured.events.utility.BaseUtility;

public class UserEventsTest extends BaseUtility {

	public Response response;
	public String accessToken;
	public String refreshToken;
	public String authLoginEndPoint = "/v1/auth/login";
	public String loginBody = "{\"email\": \"admin@mailinator.com\",\"password\": \"admin123#\",\"remember-me\": true}";
	public String emailEndPoint = "/v1/users/checkEmail";
	public String userID = "1" ;
	
	JSONObject data = new JSONObject();

	public String dataPathFile = System.getProperty("user.dir")+ "\\src\\main\\resources\\testData\\updateUser.json";

	@Test(description = "Verify login and generate access token")
	public void loginAndGenerateAccessToken(){
		response = RestAssured.given().
								contentType(ContentType.JSON).
								body(loginBody).
							when().
								post(RestAssured.baseURI + authLoginEndPoint);
		Reporter.log(response.asString());
		accessToken = response.jsonPath().get("access_token");
		Reporter.log(accessToken);
		Assert.assertEquals(response.getStatusCode(), 200);
	}
	
	@Test(description = "To authenticate with remember me for mobile")
	public void authenticateWithRememberMe() throws IOException {
		loginBody="{\"email\": \"admin@mailinator.com\",\"password\": \"admin123#\",\"remember-me\": true,\"include-in-response\": true}";
		response = RestAssured.given().
								contentType(ContentType.JSON).
								body(loginBody).
							when().
								post(RestAssured.baseURI + authLoginEndPoint);
		Reporter.log(response.asString());
		accessToken = response.jsonPath().get("access_token");
		Reporter.log(accessToken);
		refreshToken = response.jsonPath().get("refresh_token");
		Reporter.log(refreshToken);
		Assert.assertEquals(response.getStatusCode(), 200);
	}
	
	@Test(description = "Verify passed email is available or not")
	public void verifyMailAvailable() throws IOException {
		loginBody="{\"email\": \"test@example.com\"}";
		response = RestAssured.given().
								contentType(ContentType.JSON).
								accept(ContentType.JSON).
								body(loginBody).
							when().
								post(RestAssured.baseURI + emailEndPoint);
		Reporter.log(response.asString());
		Assert.assertEquals(response.getStatusCode(), 200);
		String result = response.jsonPath().get("result").toString();
		Assert.assertEquals(result,"True",loginBody+" : Email is available");
	}
	
	@Test(description = "Verify that user is able to create a new user using an email, password and an optional name")
	public void verifyCreateNewUser() throws Exception {	
		String updatedEmailID = "anchal"+RandomStringUtils.randomAlphanumeric(5)+"@test.com";
		String updatedPassword= "Test@"+RandomStringUtils.randomAlphanumeric(5);
		JSONObject attribute = new JSONObject();
		JSONObject requestJson = new JSONObject();
		attribute.put("email", updatedEmailID);
		attribute.put("password", updatedPassword);
		attribute.put("avatar_url", "http://example.com/example.png");
		attribute.put("first-name", "Anchal");
		attribute.put("last-name", "Singh");
		attribute.put("details", "ASDF");
		attribute.put("contact", "7007007007");
		attribute.put("twitter-url", "http://twitter.com/twitter");
		attribute.put("instagram-url", "http://instagram.com/instagram");
		attribute.put("facebook-url", "http://facebook.com/facebook");
		attribute.put("google-plus-url", "http://plus.google.com/plus.google");
		attribute.put("thumbnail-image-url", "http://example.com/example.png");
		attribute.put("small-image-url", "http://example.com/example.png");
		attribute.put("icon-image-url", "http://example.com/example.png");
		data.put("attributes", attribute);
		data.put("type", "user");
		data.put("id",""+userID+(int)(10.0 * Math.random()));
		requestJson.put("data", data);
		response =RestAssured.given().
							header("Authorization", "JWT " + accessToken).
							contentType("application/vnd.api+json").
							accept("application/vnd.api+json").
							body(requestJson).
						when().
							queryParam("sort", "email").
							post(RestAssured.baseURI + "/v1/users");
		Reporter.log(response.asString());
		Assert.assertEquals(response.getStatusCode(), 201);
		userID = response.jsonPath().get("data.id");
		Reporter.log("user id :: "+userID);
	}
	
	@Test(description = "To get all user details list")
	public void getUserDetails() {	
		response =RestAssured.given().
						header("Authorization", "JWT " + accessToken).
						contentType("application/vnd.api+json").
						accept("application/vnd.api+json").
						get(RestAssured.baseURI + "/v1/users/"+String.valueOf(userID));
		Reporter.log(response.getBody().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
	}
	
	@Test(description = "Verify update user Id", priority = 6)
	public void verifyUpdateUserID() throws IOException, Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		Object reader = BaseUtility.readJsonFiles("createUser.Json");
		response = RestAssured.given().
								contentType(ContentType.JSON).
								header("Authorization", "JWT " + accessToken).
								body(reader.toString()).
							when().
								patch(RestAssured.baseURI + "/v1/users/"+userID);
		Reporter.log(data.toJSONString());
		objectMapper.writeValue(
			    new FileOutputStream(dataPathFile), reader.toString());
	}
	
	@Test(description = "To delete user details")
	public void verifyDeleteUser() {
		response =RestAssured.given().
							header("Authorization", "JWT " + accessToken).
							contentType("application/vnd.api+json").
							delete(RestAssured.baseURI + "/v1/users/"+userID);
		String jsonBody = response.getBody().asString();
		Reporter.log(jsonBody);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(jsonBody.contains("Object successfully deleted"), true);
	}
		
}