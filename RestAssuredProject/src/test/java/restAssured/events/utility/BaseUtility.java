package restAssured.events.utility;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class BaseUtility {

	public static Properties properties;
	static String jsonData;
	public String userName = "";
	public String passWord = "";
	public static Response response;
	public static String accessToken;
	public static String loginBody = "{\"email\": \"admin@mailinator.com\",\"password\": \"admin123#\",\"remember-me\": true}";
	
	@BeforeTest
	public void setUp() throws IOException {
		properties = BaseUtility.readPropertyFiles();
		RestAssured.baseURI = properties.getProperty("baseURL");
		userName = properties.getProperty("UserName");
		passWord = properties.getProperty("PassWord");
	}
	
	public static Properties readPropertyFiles() throws IOException {
		properties = new Properties();
		FileInputStream fip = new FileInputStream(System.getProperty("user.dir")+ "\\src\\main\\resources\\configuration\\config.properties");
		properties.load(fip);
		return properties;
	}
	
	public static String readJsonFiles(String fileName) throws IOException, ParseException {
		FileReader fileReader = new FileReader(System.getProperty("user.dir")+ "\\src\\main\\resources\\testData\\"+fileName);
		String obj = new JSONParser().parse(fileReader).toString();	
		return obj;
	}
	
	public String getAccessToken() {
		response = RestAssured.given().
				contentType(ContentType.JSON).
				body(loginBody).
				when().
				post(RestAssured.baseURI + "/v1/auth/login");
		accessToken = response.jsonPath().get("access_token");
		Reporter.log(accessToken);
		Assert.assertEquals(response.getStatusCode(), 200);
		return accessToken;
	}
}
